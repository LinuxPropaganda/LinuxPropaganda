# Hacking and Queerness

[View Video](https://youtu.be/23ODnPgmaS4)
[Check This!](#additional-resources)



![thumbnail](copertina.jpeg)

# Video Script

## Intro
When you think about the hacking community, you probably think about guys in hoodies keymashing in a green on black terminal. While that's exactly what I'm doing right now as I wirte this script, we are in the first place people. This is the story of those people, and particularly a subset of them.

Now, this is quite the turn in video topic, so I expect to see some new audience, if you're among them, welcome! I hope you're comfortable and will enjoy the video, if so, consider joining this little thing by subscribing and entering the matrix room.

Ok, now let's start.

## Hacking culture
The hacking colture is founded on creating alternative spaces, valuing individualism, creativity, non-conformity, and, being thightly related to the free software scene, openness, collaboration, and sense of community.

These strong values naturally attracted people who were more alligned with them, and  particularly people who felt excluded from traditional spaces and social norms.

## Escape reality
For queer hackers, computers are an escape, somewhere where no one cares of what's in your pants, somewhere where you can decide how to express yourself, leaving behind the bigotry of the outside world.
Some of us are lucky enough to have someone IRL close and accepting, a group of people where to be oneself, but for others, this is the only place where they don't have to live in someone else's clothes.

## Community and Openness
Additionally, the free-as-in-freedom nature of the hacking culture, as a space where everyone can help and contribute to the communal good, makes these spaces accepting and inclusive of people from different backgrounds and identities, as everyone can contribute.
This culture of respect and tolerance has made the hacking community a welcoming space for people of all genders and sexual orientations, who have found support and solidarity within it.

## Hack Yourself
This connection is also literal, as a trans person is "hacking" their body to achieve their true self.
Those of you who programmed at least once can relate: that thrilling feeling of being able to control something you previously deemed unmodifiable, even with some sort of magical or divine property, and realizing it's just a machine at your service, that's got a lot in common with the themes of medical transition and bodily autonomy.
The connection between this concept and computer hacking is extremely visible, and the claims free software activists are applicable to trans bodies with minimal tweaking.
Note that bodily autonomy is not limited to trans healthcare, the most known example is abortion. 
I'm not gonna elaborate on this topic here but there's a great video that I linked below.

## Connection Roundup
Overall, the connection between (gender)queerness and the hacking/Linux community is based on shared values of individualism, creativity, collaboration, and acceptance. This connection is mutually benefitting, as the hacking community grows more diverse and creative, the queer community can create safe digital spaces with free resources.

## Xenia
This connection is not something new. As I anticipated at the start, this strong bond has always characterized our community, even unconciously, and this is beautifully shown by the story of Xenia, the alternative linux mascot. I'm gonna be synthetic here, but I'll leave a buncha links in description.
Basically, in 1996 Alan Mackey proposed an alternative to Tux as the Linux mascotte: a fox wearing glasses and a Linux shirt.
As Tux prevailed for its simple and cute design, Xenia was quickly forgotten, only to be rediscovered years later by a twitter user who drew fanart of this character.
Xenia was originally thought of being male, but this fanart presented her as a girl fox. The original author reached out and embraced the misunderstanding, viewing it as perfectly fitting with all his hacker friends who transitioned in the meantime.
Just like that, she became a trans rights symbol, and a brilliant way to represent our community, made of strong, independent, smart and tough, but also caring, inclusive and supportive people, with all of our quirks and peculiarities that makes the hacking scene so cool.

## Toxicity vs the "true" community
Although the linux community can seem toxic on the surface, with figures like most of Linux youtubers who are heavily right leaning, as well as the general accepted humorism in places like reddit, the actual people who make up this community are mostly extremely accepting and supportive, as hacking is a beautiful thing that should and is moved by love and not hate. Most rightist alleged "hackers" are actually just tech bros who are trying to sell you their latest crypto shit or just get you in the alt right shenanigans.
This is quickly proven by the huge variety of spaces where the community thrives Mastodon, PeerTube, Lemmy and other free social networks, which are all openly progressive and left leaning in nature, as well as the beautiful world of shared computers in the "Tildeverse", which if you don't know I urge you to check out, especially if you are learning linux right now or you wanna get started, although this doesn't mean you shouldn't if you're an expert user.

## Outro
Ok, I'll round it up for now, as I wanna get something out quickly and this topic excites me a lot so I cant be patient at all. I wrote all of this in one sitting, so it may be inprecise, if you'd like me to articulate further some of the concepts mentioned in this video, or more in general to do more of this kind of mini video essays please feel free to comment or ask in our matrix room.

Thanks a lot for whatching, I love you all.
Subscribe and share to spread the revolution!


# Definitions
In the video I used a couple of therms that may be ambiguous, here I'll try to clarify.

### Hacker
Used with the original meaning of this word, not the guy who stole your credit card.
I also refer to the linux, hacker and programming community as the same, although there's some difference.

### Individualism
As in self determination, freedom of expression and stuff like that, deatached from any economy stuff, obviously.


# Media

![Xenia Original Art](xenia.jpeg)
![meme](xenia2.jpeg)
![Original Fanart](xenia5.jpeg)


# Additional Resources

[Xenia Video](https://youtu.be/0b4eW1KAuWE)
[Bodily autonomy and transition](https://www.youtube.com/watch?v=hf1BekUXftk&t=2s)

